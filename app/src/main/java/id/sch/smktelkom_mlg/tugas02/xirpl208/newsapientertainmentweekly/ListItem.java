package id.sch.smktelkom_mlg.tugas02.xirpl208.newsapientertainmentweekly;

/**
 * Created by aliya nugraha on 2/7/2018.
 */

public class ListItem {

    private String Head;
    private String Desc;
    private String imageUrl;

    public ListItem(String head, String desc, String imageUrl) {
        Head = head;
        Desc = desc;
        imageUrl = imageUrl;
    }

    public String getHead() {
        return Head;
    }

    public String getDesc() {
        return Desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
